# GeTallele

GeTallele: a toolpack that provides a suit of functions for integrative analysis, statistical assessment and visualization of Genome and Transcriptome allele frequencies.

Files:  

Folders:  
data - folder with example of a data set generated with RNA2DNAlign (https://github.com/HorvathLab/RNA2DNAlign) and CNA generated with THetA (http://compbio.cs.brown.edu/projects/theta/)
- TCGA-BH-A0B8-01A_ad6e2482-8820-4934-bf0e-489016ce1efe.masked.seg.txt - outcome from THetA for the TCGA-BH-A0B8-01A sample
- chromosomes_lengths.mat - list of chromosome lenghts
- hg38_gene_coordinates.xlsx - excel file with gene coordinates
- import_gene_names_and_positions_from_xlsx.m - Matlab function to import the hg38_gene_coordinates.xlsx file
- known_rs_013readcounts.tsv.zip - compressed outcome of the RNA2DNAlign for the TCGA-BH-A0B8-01A sample (contains VAFs and readcounts for normal exome, normal transcriptome, tumor exome and tumor transcriptome). Unzip to use.

circos - Matlab functions and script to generate circos plots with VAFs and vprs  
circos\circos_conf_files - config files for generating layout of the circos plots